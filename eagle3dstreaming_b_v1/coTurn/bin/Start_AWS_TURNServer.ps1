

$publicIp = Invoke-RestMethod http://ipinfo.io/json | Select -exp ip

#aws
#import-module C:\programdata\amazon\ec2-windows\launch\Module\Ec2Launch.psm1 ; add-routes
$LocalIp = Invoke-WebRequest -Uri "http://169.254.169.254/latest/meta-data/local-ipv4"


#azure
#https://docs.microsoft.com/en-us/azure/virtual-machines/windows/instance-metadata-service
#$LocalIp = Invoke-RestMethod -Headers @{"Metadata"="true"} -Method GET  -Uri "http://169.254.169.254/metadata/instance/network?api-version=2017-08-01"
# $publicIp =Invoke-RestMethod -Headers @{"Metadata"="true"} -Method GET  -Uri "http://169.254.169.254/metadata/instance/network/interface/0/ipv4/ipAddress/0/publicIpAddress?api-version=2017-08-01&format=text"
# Write-Output "publicIp IP: $publicIp"
# $LocalIp = Invoke-RestMethod -Headers @{"Metadata"="true"} -Method GET  -Uri "http://169.254.169.254/metadata/instance/network/interface/0/ipv4/ipAddress/0/privateIpAddress?api-version=2017-08-01&format=text"



#https://stackoverflow.com/questions/53664145/need-help-for-coturn-configuration-for-my-public-ip-address
#sudo turnserver -v -o -a -user username:key -f -L private_ip -X public_ip -E private_ip -min-port=minport_number -max-port=max_port_number -r public_ip --no-tls --no-dtls



Write-Output "Private IP: $LocalIp"


$ProcessExe = ".\turnserver.exe"
#$Arguments = "-L private_IP -X public_IP"

$Arguments = -join("-L", " ", $LocalIp," ","-X", " ", $publicIp);




Write-Output "Running: $ProcessExe $Arguments"
Start-Process -FilePath $ProcessExe -ArgumentList $Arguments
